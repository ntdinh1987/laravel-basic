<?php

namespace App\Http\Controllers;

use App\Models\Skill;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function index()
    {

        $slides = [
            [
                'title' => 'This my porfolio',
                'slogan' => 'Tổng hợp thông tin'
            ],
            [
                'title' => 'I\'m Nguyen The Dinh',
                'slogan' => 'This is my porfolio'
            ],
        ];

        //Lấy dữ liệu từ bảng skilss
        //Lấy tất cả dữ liệu từ bảng skills
        $skills = Skill::take(4)->get();

        return view('home', [
            'sliders' => $slides,
            'skills' => $skills
        ]);
    }
}
