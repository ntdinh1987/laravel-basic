<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Skill;
use Illuminate\Http\Request;

class SkillController extends Controller
{

    public function index()
    {
        
        $skills = Skill::all();
        
        return view('admin.skills.index', ['skills' => $skills]);
    }

    public function add()
    {

        return view('admin.skills.add');
    }

    public function postAdd(Request $request)
    {

        $skill = new Skill();
        $skill->title = $request->title;
        $skill->level = $request->level;

        $skill->save();

        return redirect()->route('admin.skills.index');

    }
}
