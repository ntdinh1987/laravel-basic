Danh sách skills

<a href="{{route('admin.skills.add')}}">Thêm mới</a>

<table border="1" width="100%">
    <thead>
        <tr>
            <td>ID</td>
            <td>Tiêu đề</td>
            <td>Cấp độ</td>
        </tr>
    </thead>
    <tbody>

        @foreach ($skills as $skill)
        <tr>
            <td>{{$skill->id}}</td>
            <td>{{$skill->title}}</td>
            <td>{{$skill->level}}</td>
        </tr>
        @endforeach

    </tbody>

</table>


    
