<?php

use App\Http\Controllers\Admin\SkillController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);

Route::prefix('admin')->group(function () {
    
    Route::prefix('skills')->group(function () {
        
        Route::get('/', [SkillController::class, 'index'])->name('admin.skills.index');
        Route::get('/add', [SkillController::class, 'add'])->name('admin.skills.add');
        Route::post('/add', [SkillController::class, 'postAdd']);

    });

});

//http://domian.com/admin/skills


